﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;

[Serializable]
// Set up all of the stuff to be sent to file here
public struct GameStatus {
	public string playerName, playerClass;
	public bool mouseInv;
	public float difficulty;
}

public class jsonController : MonoBehaviour {

	// initialise variables and game objects, as 
	// well as point Unity to where we want the file stored
	const string SAVE_FILENAME = "gameStatus.txt";
	string filePath;

	public GameObject 
		cameraObj,
		sliderObj,
		inputFieldObj,
		nameObj,
		sliderTextObj,
		dropdownObj,
		toggleObj,
	    toggleLabelObj;

	GameStatus currentGame;

	// fired when saved
	public void SaveEvent () {
		Debug.Log ("Save Clicked!");
		string serGameStatus = JsonUtility.ToJson (currentGame);
		File.WriteAllText (filePath + "/" + SAVE_FILENAME, serGameStatus);
	}

	//fired when loaded
	public void LoadEvent () {
		Debug.Log ("Load Clicked!");
		if (File.Exists (filePath + "/" + SAVE_FILENAME)) {
			string jsonGameStatus = File.ReadAllText (filePath + "/" + SAVE_FILENAME);
			currentGame = JsonUtility.FromJson<GameStatus> (jsonGameStatus);
			UpdateAll ();
		} else {
			Debug.Log ("File not found");
		}
	}

	// Put the stuff for the player name and class in a separate function so it can be called more easily.
	public void TextUpdate () {
		Text h = nameObj.GetComponent<Text> ();
		h.text = currentGame.playerName + " " + currentGame.playerClass;
	}

	// resets variables (though not scene), also run at start to give a clean slate.
	public void ResetEvent () {
		currentGame.playerName = "Player";
		currentGame.playerClass = "";
		currentGame.difficulty = 1;
		currentGame.mouseInv = false;
	}

	// Player name is updated here, then generic TextUpdate() is called.
	public void NameUpdate () {
		InputField n = inputFieldObj.GetComponent<InputField> ();
		currentGame.playerName = n.text;
		TextUpdate ();
	}

	// Dropdown menu for player's class selection here, as above, TextUpdate() 
	// is called afterwards as well.
	public void DropdownUpdate () {
		Dropdown d = dropdownObj.GetComponent<Dropdown> ();

		if (d.value == 1) {
			currentGame.playerClass = "(Soldier)";
		} else if (d.value == 2) {
			currentGame.playerClass = "(Sniper)";
		} else if (d.value == 3) {
			currentGame.playerClass = "(Medic)";
		} else if (d.value == 4) {
			currentGame.playerClass = "(Support)";
		} else {
			currentGame.playerClass = "";
		}
		TextUpdate ();
	}

	//Set in the inspector to run this function onValueChange
	public void SliderUpdate () {

		//Grab necessary components
		float v = sliderObj.GetComponent<Slider> ().value;
		Text t = sliderTextObj.GetComponent<Text> ();

		if (v == 1) {
			t.text = "Story Mode - In this mode, the game difficulty is greatly reduced. Even the most novice of players will be able to play the game, soaking up the story, without having to watch the healthbar.";
		} else if (v == 2) {
			t.text = "Easy - You play the game mostly for the story, but a little challange is nice too.";
		} else if (v == 3) {
			t.text = "Normal - Default setting. A healthy mix of story elements and challanging first-person combat, for veteran players.";
		} else if (v == 4) {
			t.text = "Hard - Enemies are quicker to engage, and have better accuracy than your typical NPCs. Not for the feint of heart.";
		} else {
			t.text = "Insane - You will die. It's a question of \"When\" not \"If\".";
		}

		//Also assign to struct to be used with JSON
		currentGame.difficulty = v;
	}

	//Set in the inspector to run onValueChange
	public void toggleUpdate () {

		//grabbing necessary GOs. Annoying that a second GO is required for text, but hey
		bool toggleSelected = toggleObj.GetComponent<Toggle> ().isOn;
		Text toggleText = toggleLabelObj.GetComponent<Text> ();

		if (toggleSelected == true || currentGame.mouseInv == true) {
			currentGame.mouseInv = true;
			toggleText.text = "Inverted";
		} else {
			currentGame.mouseInv = false;
			toggleText.text = "Not inverted";
		}

	}

	// Something to call to get everything displaying nicely whenever 
	// load, save or start is called.
	void UpdateAll () {
		toggleUpdate ();
		SliderUpdate ();
		DropdownUpdate ();
		TextUpdate ();
	}
		
	void Start () {
		currentGame = new GameStatus ();
		filePath = Application.persistentDataPath;
		ResetEvent ();
	}
}
