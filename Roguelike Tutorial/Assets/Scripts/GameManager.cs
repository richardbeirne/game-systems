﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public BoardManager boardScript;
	public static GameManager instnace = null;

	public int level = 3;

	// Use this for initialization
	void Awake () {
		if (instnace == null) {
			instnace = this;
		} else if (instnace != null) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
		boardScript = GetComponent<BoardManager> ();
		InitGame();
	}

	void InitGame () {
		boardScript.SetupScene (level);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
