﻿using UnityEngine;
using System.Collections;

public class animationController : MonoBehaviour {

	float hInput, vInput;
	Animator animatorControllerObj;

	// Use this for initialization
	void Start () {
		animatorControllerObj = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		hInput = Input.GetAxis ("Horizontal");
		vInput = Input.GetAxis ("Vertical");
		animatorControllerObj.SetFloat ("hInput", hInput);
		animatorControllerObj.SetFloat ("vInput", vInput);
	}
}
