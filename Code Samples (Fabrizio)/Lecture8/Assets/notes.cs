﻿using UnityEngine;
using System.Collections;

public class notes : MonoBehaviour {

	/* Lecture 8 notes
	 * 
	 * Create animation controller from Asset menu. Attach to prefab of teddy bear.
	 * Create states for different animations, attaching an animation to each state in the animation inspector
	 * 
	 * To blend animations, make transitions between states (still dunno how I did this, lots of clicks randomly)
	 * Make a script, get animator reference to a perameter made in the animator inspector and change it via keyboard input
	 * 
	 * To use layer massk, create a new layer mask, select the parts you want to exclude (They'll appear in red), and save it.
	 * You can then apply this mask to a new layer. In the teddy bear example only top half was selected (weird MJ arms), then
	 * this was applied to the base layer. To get that to work you have to put the weight as 1, and set it to 'override'. Setting
	 * 'syncing' doesn't appear to do much, but I left it selected in this example anyway.
	 * 
	 * To get that feet spacing, select the prefab, click on 'configure', then under 'muscles and settings' you can adjust
	 * the 'feet spacing' slider (along with a bunch of others that are bi-ped specific animation variables.
	 */

}
