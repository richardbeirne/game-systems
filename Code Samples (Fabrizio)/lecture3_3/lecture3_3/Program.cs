﻿using System;

namespace lecture3_3
{
	public class Student{
		public string name, ID;
		private int[] examResults;

		public int[] Results
		{
			set{
				examResults = value;
			}
			get{
				return examResults;
			}
		}

		public string semesterMark()
		{
			int resultsTotal = 0;
			for(int i=0; i<examResults.Length; i++){
				resultsTotal += examResults[i];
			}

			if(resultsTotal < (examResults.Length * 40))
			{
				return "Failed";
			} else{
				return "Passed";
			}
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			Student fabrizio = new Student ();
			Student keith = new Student ();

			int[] fabMarks = {30,35,40};
			int[] kiethMarks = {55,65,70};

			fabrizio.name = "Fabrizio";
			fabrizio.Results = fabMarks;
			keith.name = "Keith";
			keith.Results = kiethMarks;



			Console.WriteLine("{0}: {1}", fabrizio.name, fabrizio.semesterMark());
		}

	}
}
