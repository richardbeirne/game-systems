﻿using UnityEngine;
using System.Collections;

public class playAudio : MonoBehaviour {

	public GameObject player;
	public float distanceThreshold;

	private float distance;

	// Update is called once per frame
	void Update () {
		distance = Vector3.Distance (player.transform.position, transform.position);

		if (distance < distanceThreshold && !GetComponent<AudioSource> ().isPlaying) {
			GetComponent<AudioSource> ().Play();
		}
	}
}
