﻿using UnityEngine;
using System.Collections;

public class InputStateController : MonoBehaviour {

	//setup a variable to point to the Animator Controller for the character]
	Animator animator;
	//and another variable to hold the input value
	float vertInput;
	float horizInput;

	// Use this for initialization
	void Start () {
		//get the Animator Controller Component from the character component hierarchy
		animator = GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	void Update () {
		//get the input from the vertical axis
		vertInput = Input.GetAxis("Vertical");
		horizInput = Input.GetAxis("Horizontal");
	}
	void FixedUpdate(){
		//use fixed update to control the animation as it will behave in real time
		//now set the animator float value (vAxisInput) with the input value
		//animator.SetFloat ("vAxisInput", vertInput, 0.1f, Time.deltaTime);
		animator.SetFloat ("vAxisInput", vertInput);
		animator.SetFloat ("hAxisInput", horizInput);
	}
}
