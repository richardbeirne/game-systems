﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {

	public GameObject target;
	public float cameraHeight, cameraDistance;
	public float distanceDamping, rotationDamping;

	float currentHeight, targetHeight;
	float currentRotation, targetRotation;

	// Update is called once per frame
	void Update () {
		if (target == null) {
			return;
		}

		currentHeight = transform.position.y;
		currentRotation = Mathf.LerpAngle(currentRotation, targetRotation, rotationDamping * Time.deltaTime);
		currentHeight = Mathf.Lerp (currentHeight, targetHeight, distanceDamping * Time.deltaTime);

		targetHeight = target.transform.position.y + cameraHeight;
		targetRotation = target.transform.eulerAngles.y;

		Quaternion cameraRotation = Quaternion.Euler (0.0f, currentRotation, 0.0f);

		transform.position = target.transform.position;
		transform.position -= cameraRotation * Vector3.forward * cameraDistance;
		transform.position = new Vector3 (transform.position.x, currentHeight, transform.position.z);

		transform.LookAt (target.transform);
	}
}
