﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class AudioUIManager : MonoBehaviour {

	public AudioMixer gameAudio;

	const float MIN_VOLUME = -80.0f, MAX_VOLUME = 0.0f;

	public void MuteMaster(bool isChecked){
		if (isChecked) {
			gameAudio.SetFloat ("MasterVolume", MIN_VOLUME);
		} else {
			gameAudio.SetFloat ("MasterVolume", MAX_VOLUME);
		}
	}

	public void MuteMusic(bool isChecked){
		if (isChecked) {
			gameAudio.SetFloat ("MusicVolume", MIN_VOLUME);
		} else {
			gameAudio.SetFloat ("MusicVolume", MAX_VOLUME);
		}
	}

	public void MuteSFX(bool isChecked){
		if (isChecked) {
			gameAudio.SetFloat ("SFXVolume", MIN_VOLUME);
		} else {
			gameAudio.SetFloat ("SFXVolume", MAX_VOLUME);
		}
	}
}