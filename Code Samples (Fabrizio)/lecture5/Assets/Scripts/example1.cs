﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class example1 : MonoBehaviour {

	public GameObject uiTextGO;
	public GameObject uiSliderGO;

	public void sliderUpdate (float v) {
		Text uiText = uiTextGO.GetComponent<Text> ();
		Renderer uiSlider = uiSliderGO.GetComponent<Renderer> ();

		int sliderValue = Mathf.RoundToInt (v * 100f);
		uiText.text = sliderValue.ToString();
		Debug.Log (sliderValue);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
