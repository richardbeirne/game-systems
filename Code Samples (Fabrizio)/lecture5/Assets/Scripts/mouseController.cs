﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class mouseController : MonoBehaviour {

	public GameObject mousePosX;
	public GameObject mousePosY;

	Text mouseTextX;
	Text mouseTextY;

	float xPos, yPos;

	// Use this for initialization
	void Start () {
		mouseTextX = mousePosX.GetComponent<Text> ();
		mouseTextY = mousePosY.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		xPos = Input.GetAxis ("Mouse X");
		yPos = Input.GetAxis ("Mouse Y");

		mouseTextX.text = "X: " + xPos.ToString ();
		mouseTextY.text = "Y: " + yPos.ToString ();
	}
}
