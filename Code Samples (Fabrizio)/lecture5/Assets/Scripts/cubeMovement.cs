﻿using UnityEngine;
using System.Collections;

public class cubeMovement : MonoBehaviour {

	public GameObject sphereGameObject;
	public sphereMovement sphereScript;

	// Use this for initialization
	void Start () {
		sphereScript = sphereGameObject.GetComponent<sphereMovement> ();
	}
	
	// Update is called once per frame
	void Update () {
		sphereScript.UpdateSpherePosition (0.2f);
	}
}
