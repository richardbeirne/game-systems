﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class controlManager : MonoBehaviour {

	public GameObject player, cubeX, cubeY, cubeZ;
	float xInput, yInput, zInput, speed = 10.0f;
	Text cubeXText, cubeYText, cubeZText;

	// Use this for initialization
	void Start () {
		cubeXText = cubeX.GetComponent<Text> (); 
		cubeYText = cubeY.GetComponent<Text> ();
		cubeZText = cubeZ.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		xInput = Input.GetAxis ("Horizontal");
		yInput = Input.GetAxis ("Vertical");
		zInput = Input.GetAxis ("Mouse Y");

		player.transform.position = new Vector3 (
			player.transform.position.x + xInput * Time.deltaTime * speed,
			player.transform.position.y + yInput * Time.deltaTime * speed,
			player.transform.position.z + zInput);

		cubeXText.text = "X: " + player.transform.position.x.ToString ();
		cubeYText.text = "Y: " + player.transform.position.y.ToString ();
		cubeZText.text = "Z: " + player.transform.position.z.ToString ();
	}
}
