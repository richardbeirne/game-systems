﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	float lifepoints = 100;
	float hAxisInput, vAxisInput;

	const float PLAYER_VELOCITY = 10.0f;
	float frameSpeed;

	public float Life {
		get{
			return lifepoints;
		}
		set{
			lifepoints = value;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		hAxisInput = Input.GetAxis ("Horizontal");
		vAxisInput = Input.GetAxis ("Vertical");

		if (lifepoints <= 0.0f) {
			Destroy (gameObject);
		}

		frameSpeed = PLAYER_VELOCITY * Time.deltaTime;

		Vector3 speedVector = new Vector3 (hAxisInput, 0.0f, vAxisInput);
		speedVector.Normalize();
		speedVector *= frameSpeed;

		GetComponent<Rigidbody>().AddForce(speedVector, ForceMode.VelocityChange);
	}
}
