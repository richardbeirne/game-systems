﻿using UnityEngine;
using System.Collections;

public class NPCController : MonoBehaviour {

	PlayerController playerScript;
	WaypointManager patrolManager;

	public GameObject cam;
	GameObject target;

	const float 
		MAX_SPEED = 11.0f, 
		SCALE_FACTOR = 0.75f, 
		MAX_WAYPOINT_DIST = 1.0f;
	float AiFrameSpeed;
	Vector3 speedVector, targetPosition;

	// Use this for initialization
	void Start () {
		patrolManager = cam.GetComponent<WaypointManager> ();
		target = patrolManager.nextWaypoint (null);
	}

	// Update is called once per frame
	void FixedUpdate () {

		if (Vector3.Distance(target.transform.position, transform.position) < MAX_WAYPOINT_DIST){
			target = patrolManager.nextWaypoint (target);
		}

		AiFrameSpeed = MAX_SPEED * Time.deltaTime;

		// --- Behavior type selection ---
		//speedVector = Seek (transform.position, targetPosition, AiFrameSpeed);
		speedVector = Arrive (transform.position, target.transform.position);

		GetComponent<Rigidbody> ().AddForce (speedVector, ForceMode.VelocityChange);
	}

	void OnCollisionEnter(Collision col){
		
	}

	Vector3 Seek(Vector3 source, Vector3 target, float movingSpeed){
		Vector3 directionToTarget = (target - source);
		directionToTarget.Normalize ();
		Vector3 speedToTarget = directionToTarget *= movingSpeed;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}

	Vector3 Arrive(Vector3 source, Vector3 target){
		float distanceToTarget = Vector3.Distance (source, target);
		Vector3 directionToTarget = (target - source);
		directionToTarget.Normalize ();

		float speedScaler = distanceToTarget / SCALE_FACTOR * Time.deltaTime;
		Vector3 speedToTarget = directionToTarget * speedScaler;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}
}