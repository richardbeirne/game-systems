﻿using UnityEngine;
using System.Collections;

public class SmartSphere : MonoBehaviour {

	NavMeshAgent navAgent;
	public GameObject target;

	// Use this for initialization
	void Start () {
		navAgent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		navAgent.SetDestination (target.transform.position);
	}
}
