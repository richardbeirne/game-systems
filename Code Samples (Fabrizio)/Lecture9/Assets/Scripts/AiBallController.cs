﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AiBallController : MonoBehaviour {

	public GameObject target, textUI;

	PlayerController playerScript;

	const float MAX_SPEED = 11.0f, SCALE_FACTOR = 0.75f;
	float AiFrameSpeed;
	Vector3 speedVector, targetPosition;

	// Use this for initialization
	void Start () {
		playerScript = target.GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (target == null) {
			targetPosition = Vector3.zero;
		} else {
			targetPosition = target.transform.position;
		}

		textUI.GetComponent<Text> ().text = "Life Points: " + playerScript.Life;
		AiFrameSpeed = MAX_SPEED * Time.deltaTime;

		// --- Behavior type selection ---
		//speedVector = Seek (transform.position, targetPosition, AiFrameSpeed);
		speedVector = Arrive (transform.position, targetPosition);

		GetComponent<Rigidbody> ().AddForce (speedVector, ForceMode.VelocityChange);
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.name == "Player") {
			playerScript.Life -= 20;
			Destroy (gameObject);
		}
	}

	Vector3 Seek(Vector3 source, Vector3 target, float movingSpeed){
		Vector3 directionToTarget = (target - source);
		directionToTarget.Normalize ();
		Vector3 speedToTarget = directionToTarget *= movingSpeed;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}

	Vector3 Arrive(Vector3 source, Vector3 target){
		float distanceToTarget = Vector3.Distance (source, target);
		Vector3 directionToTarget = (target - source);
		directionToTarget.Normalize ();

		float speedScaler = distanceToTarget / SCALE_FACTOR * Time.deltaTime;
		Vector3 speedToTarget = directionToTarget * speedScaler;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}
}
