﻿using UnityEngine;
using System.Collections;

public class RandomBall : MonoBehaviour {

	const float SPEED = 40.0f;
	Vector3 direction;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void FixedUpdate () {
		int randomDirection = Random.Range (0, 4);

		switch (randomDirection) {
		case 0:
			direction = Vector3.back;
			break;
		case 1:
			direction = Vector3.left;
			break;
		case 2:
			direction = Vector3.forward;
			break;
		case 3:
			direction = Vector3.right;
			break;
		}

		Vector3 speedVector = direction * SPEED * Time.deltaTime;
		GetComponent<Rigidbody> ().AddForce (speedVector, ForceMode.Impulse);
	}
}
