﻿using UnityEngine;
using System.Collections;

public class WaypointManager : MonoBehaviour {

	public GameObject[] waypointList;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject nextWaypoint(GameObject currentWaypoint){
		int nextWayPointIndex = 0;

		if (currentWaypoint == null) {
			Debug.Log ("Persuing: " + waypointList [nextWayPointIndex].name);
			return waypointList [nextWayPointIndex];
		}

		for (int i = 0; i < waypointList.Length; i++) {
			if (currentWaypoint == waypointList [i]) {
				nextWayPointIndex = ((i + 1) % waypointList.Length);
			}
		}
		Debug.Log ("Persuing: " + waypointList [nextWayPointIndex].name);
		return waypointList [nextWayPointIndex];
	}
}
