﻿using UnityEngine;
using System.Collections;

public class lecture4_1 : MonoBehaviour {

	private float xPos;
	public float speed = 0.1f;
	private float move;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		xPos = transform.position.x;
		move = xPos + speed;
		transform.position = new Vector3 (move, 0, 0);

		Debug.Log ("Position from origin is: "+move);
	}
}
