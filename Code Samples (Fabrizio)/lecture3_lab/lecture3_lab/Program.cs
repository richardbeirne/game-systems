﻿using System;

namespace lecture3_lab
{
	class MainClass
	{		
		public class playerCharacter
		{
			private int playerHealth = 100;

			public int Health
			{
				get{
					return playerHealth;
				}
				set{
					playerHealth = value;
				}
			}
		}

		public class enemy
		{
			public playerCharacter Player;
			private int enemyHealth;

			public int Health
			{
				get{
					return enemyHealth;
				}
				set{
					enemyHealth = value;
				}
			}

			public void dealPlayerDamage(){
				Player.Health -= 10;
			}
		}
			

		public static void Main (string[] args)
		{
			playerCharacter Player = new playerCharacter ();
			enemy EnemyOne = new enemy ();

			EnemyOne.Player = Player;
		}
	}
}
