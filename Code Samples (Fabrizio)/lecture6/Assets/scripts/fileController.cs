﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;

public class fileController : MonoBehaviour {

	string playerName = "Fabrizio";
	int coins = 0, hp = 100;

	const string SAVE_FILENAME = "gameStatus.txt";
	string filePath;

	public GameObject gameStatusUIObj;

	public void RandomEvent () {
		hp = UnityEngine.Random.Range (0, 10) + hp;
	}
	public void SaveEvent () {
		SaveGameStatus ();
	}
	public void LoadEvent () {
		LoadGameStatus ();
	}

	void ShowGameStatus () {
		string uiMessage = 
			"Player name: " + playerName + "\n" +
			"Coins: " + coins + "\n" +
			"HP: " + hp;

		gameStatusUIObj.GetComponent<Text> ().text = uiMessage;
	}

	void SaveGameStatus () {
		File.WriteAllText (filePath + "/" + SAVE_FILENAME, hp.ToString());
		Debug.Log (filePath);
	}

	void LoadGameStatus () {
		if (File.Exists (filePath + "/" + SAVE_FILENAME)) {
			string hpString = File.ReadAllText (filePath + "/" + SAVE_FILENAME);
			hp = (int)Double.Parse (hpString);
		} else {
			Debug.Log ("File not found");
		}
	}

	void Awake() {
		Debug.Log ("I'm awake");
	} 
		
	void Start () {
		filePath = Application.persistentDataPath;
	}

	void Update () {
		ShowGameStatus ();
	}

	void OnApplicationPause (bool isPaused) {
		if (isPaused) {
			Debug.Log ("I'm paused");
			SaveGameStatus ();
		} else {
			Debug.Log ("I've resumed");
			LoadGameStatus ();
		}
	}

	void OnApplicationQuit () {
		Debug.Log ("I've quit");
		SaveGameStatus ();
	}
}
