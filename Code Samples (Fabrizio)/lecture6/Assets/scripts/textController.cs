﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class textController : MonoBehaviour {

	string playerName = "Fabrizio";
	int coins = 0, hp = 100;

	public GameObject gameStatusUIObj;

	void ShowGameStatus () {
		string uiMessage = 
			"Player name: " + playerName + "\n" +
			"Coins: " + coins + "\n" +
			"HP: " + hp;

		gameStatusUIObj.GetComponent<Text> ().text = uiMessage;
	}

	void SetGameStatus () {
		PlayerPrefs.SetString ("playerName", playerName);
		PlayerPrefs.SetInt ("coins", coins);
		PlayerPrefs.SetInt ("hp", hp);
		PlayerPrefs.Save ();
	}

	void GetGameStatus () {
		playerName = PlayerPrefs.GetString ("playerName");
		coins = PlayerPrefs.GetInt ("coins");
		hp = PlayerPrefs.GetInt ("hp");
	}

	void Awake() {
		Debug.Log ("I'm awake");
	} 
		
	void Start () {
	}

	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			coins++;
		}
		if (Input.GetButtonDown ("Fire2")) {
			hp--;
		}	
		ShowGameStatus ();
	}

	void OnApplicationPause (bool isPaused) {
		if (isPaused) {
			Debug.Log ("I'm paused");
			SetGameStatus ();
		} else {
			Debug.Log ("I've resumed");
			GetGameStatus ();
		}
	}

	void OnApplicationQuit () {
		Debug.Log ("I've quit");
		SetGameStatus ();
	}
}
