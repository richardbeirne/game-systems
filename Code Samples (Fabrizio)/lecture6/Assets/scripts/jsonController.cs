﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;

[Serializable]
// Set up all of the stuff to be sent to file here
public struct GameStatus {
	public string playerName;
	public float hp, difficulty;
}

public class jsonController : MonoBehaviour {

	//initialise variables and game objects
	const string SAVE_FILENAME = "gameStatus.txt";
	string filePath, pName, pClass;

	public GameObject 
		cameraObj,
		sliderObj,
		inputFieldObj,
		nameObj,
		sliderTextObj,
		dropdownObj,
		toggleObj,
	    toggleLabelObj;

	GameStatus currentGame;

	//random event to alter the states of some variables for easy tracking. Might take this out.
	public void RandomEvent () {
		Debug.Log ("Random Clicked!");
		currentGame.hp = UnityEngine.Random.Range (0, 10) + currentGame.hp;
	}

	//fired when saved
	public void SaveEvent () {
		Debug.Log ("Save Clicked!");
		string serGameStatus = JsonUtility.ToJson (currentGame);
		File.WriteAllText (filePath + "/" + SAVE_FILENAME, serGameStatus);
	}

	//fired when loaded
	public void LoadEvent () {
		Debug.Log ("Load Clicked!");
		if (File.Exists (filePath + "/" + SAVE_FILENAME)) {
			string jsonGameStatus = File.ReadAllText (filePath + "/" + SAVE_FILENAME);
			currentGame = JsonUtility.FromJson<GameStatus> (jsonGameStatus);
		} else {
			Debug.Log ("File not found");
		}
	}

	public void TextUpdate () {
		Text h = nameObj.GetComponent<Text> ();
		h.text = pName + " " + pClass;
		Debug.Log (h.text);
	}

	// resets variables (though not scene)
	public void ResetEvent () {
		SetVariables ();
	}

	public void NameUpdate () {
		InputField n = inputFieldObj.GetComponent<InputField> ();
		string t = n.text.ToString ();
		Debug.Log (t + "FUCK");
		pName = n.text;
		TextUpdate ();
	}

	public void DropdownUpdate () {
		Dropdown d = dropdownObj.GetComponent<Dropdown> ();

		if (d.value == 1) {
			pClass = "(Soldier)";
		} else if (d.value == 2) {
			pClass = "(Sniper)";
		} else if (d.value == 3) {
			pClass = "(Medic)";
		} else if (d.value == 4) {
			pClass = "(Support)";
		} else {
			pClass = "";
		}
		TextUpdate ();
	}

	//Set in the inspector to run this function onValueChange
	public void SliderUpdate () {

		//Grab necessary components
		float v = sliderObj.GetComponent<Slider> ().value;
		Text t = sliderTextObj.GetComponent<Text> ();
		//Also assign to struct to be used with JSON
		currentGame.difficulty = v;

		if (v == 1) {
			t.text = "Story Mode - In this mode, the game difficulty is greatly reduced. Even the most novice of players will be able to play the game, soaking up the story, without having to watch the healthbar.";
		} else if (v == 2) {
			t.text = "Easy - You play the game mostly for the story, but a little challange is nice too.";
		} else if (v == 3) {
			t.text = "Normal - Default setting. A healthy mix of story elements and challanging first-person combat, for veteran players.";
		} else if (v == 4) {
			t.text = "Hard - Enemies are quicker to engage, and have better accuracy than your typical NPCs. Not for the feint of heart.";
		} else {
			t.text = "Insane - You will die. It's a question of \"When\" not \"If\".";
		}
	}

	//Set in the inspector to run onValueChange
	public void toggleUpdate () {

		//grabbing necessary GOs. Annoying that a second GO is required for text, but hey
		bool toggleSelected = toggleObj.GetComponent<Toggle> ().isOn;
		Text toggleText = toggleLabelObj.GetComponent<Text> ();

		if (toggleSelected == true) {
			Debug.Log (toggleSelected);
			toggleText.text = "Inverted";
		} else {
			Debug.Log (toggleSelected);
			toggleText.text = "Not inverted";
		}

	}

	//Put this in its own function for neatness.
	void SetVariables () {
		currentGame.playerName = "Player";
		currentGame.hp = 100;
	}

	void Start () {
		currentGame = new GameStatus ();
		filePath = Application.persistentDataPath; 

		//run these all here to get everything displaying properly on first run.
		SetVariables ();
		toggleUpdate ();
		SliderUpdate ();

	}

	void Update () {
		
	}
}
