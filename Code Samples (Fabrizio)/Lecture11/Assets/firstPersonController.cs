﻿
using UnityEngine;
using System.Collections;

public class firstPersonController : MonoBehaviour {

	public float xMoveSensitivity, zMoveSensitivity, xLookSensitivity, yLookSensitivity;

	private float xMovement, zMovement, xMouse, yMouse;

	private Vector3 movementVector;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		xMovement = Input.GetAxis ("Horizontal");
		zMovement = Input.GetAxis ("Vertical");
		xMouse = Input.GetAxis ("Mouse X");
		yMouse = Input.GetAxis ("Mouse Y");

		movementVector = new Vector3(
			xMovement * xMoveSensitivity * Time.deltaTime,
			0.0f,
			zMovement * zMoveSensitivity * Time.deltaTime);

		transform.position += movementVector;

		transform.Rotate (Vector3.up, xMouse * xLookSensitivity * Time.deltaTime * 10);
		transform.Rotate (Vector3.left, yMouse * yLookSensitivity * Time.deltaTime * 10);
	}
}
