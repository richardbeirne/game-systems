﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class UIController : MonoBehaviour {

	//Reference to UI gameobjects
	public Text scoreText, levelText;

	//grab the stateController so we can use its values
	private StateController stateController;

	private int currentScore, currentLevel;

	void Start () {
		stateController = GetComponent<StateController> ();
	}
	
	// Update is called once per frame
	void Update () {

		//update the values of necessary elements from GameController
		currentScore = stateController.gameStats.currentScore;
		currentLevel = stateController.gameStats.currentLevel;

		//Apply those updates to text elements on-screen
		scoreText.text = "SCORE: " + currentScore;
		levelText.text = "LEVEL: " + currentLevel;
	}
}
