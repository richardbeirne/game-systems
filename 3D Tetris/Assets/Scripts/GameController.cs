﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour {

	//references to block types, and grid size.
	public GameObject[] blocks; 
	public GameObject 
		player,
		spawner, 
		boundary, 
		instantiatedBlock;
	public int gridSize;
	private float blockSpawnHeight;

	//parent gameobject to hold spawners in hierarchy, and array for reference here.
	private GameObject[] spawners, detectors;

	//vars for control spawning speed of blocks.
	public float blockSpawnRate;
	private float nextSpawn;

	//Array to hold the transforms of the cubes making up each block.
	private Transform[] blockTransforms;

	//player data
	private StateController stateController;
	private int currentScore, currentLevel;

	//an areWeCurrentlyPaused check
	private bool isPaused = false;

	void Awake() {
		//Setup player state data
		SetupPlayerData ();

		//Set overall height for blocks to spawn in the game.
		blockSpawnHeight = 40.0f;

		//Instantiate the boundary collider with the correct position and size.
		SetupBoundary ();

		//Instantiate the spawning locations for all blocks.
		SetupSpawners ();

		//Instantiate the colliders in every possible block location to act as detectors.
		SetupDetectors ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire2") && Time.time > nextSpawn) {
			SpawnBlock ();
			stateController.gameStats.currentScore++;
		}

		if (Input.GetButton ("Cancel")) {
			Debug.Log ("You just hit escape!");
			if (isPaused) {
				Time.timeScale = 1;

			} else {
				Time.timeScale = 0;
			}

		}
	}

	void SpawnBlock() {
		nextSpawn = Time.time + blockSpawnRate;
		//Take a random block type, and spawn it in a random spawner.
		instantiatedBlock = Instantiate (blocks[Random.Range(0, blocks.Length)], spawners[Random.Range(0, spawners.Length)].transform.position, Quaternion.identity) as GameObject;

		//Keep block in the boundary location by checking the location of each cube in a block prefab
		//and making sure it's inside the boundary collider.
		blockTransforms = instantiatedBlock.GetComponentsInChildren<Transform> ();
		foreach (Transform subBlockTransform in blockTransforms) {
			Transform parentTransform = subBlockTransform.GetComponentInParent<Transform> ();

			//This is a disgusting mess. Need to come back and fix this.
			if (subBlockTransform.position.x > gridSize * 2) {
				parentTransform.position = new Vector3 (gridSize * 2, parentTransform.position.y, parentTransform.position.z);
			} else if (subBlockTransform.position.x < 1) {
				parentTransform.position = new Vector3 (1.0f, parentTransform.position.y, parentTransform.position.z);
			} else if (subBlockTransform.position.z > gridSize * 2) {
				parentTransform.position = new Vector3 (parentTransform.position.x, parentTransform.position.y, gridSize * 2);
			} else if (subBlockTransform.position.z < 1) {
				parentTransform.position = new Vector3 (parentTransform.position.x, parentTransform.position.y, 1.0f);
			}
		}
	}

	//Get reference to StateController, and pull in needed player data, also set reference to player object, as we're here
	void SetupPlayerData () {
		stateController = GetComponent<StateController> ();

		currentScore = stateController.gameStats.currentScore;
		currentLevel = stateController.gameStats.currentLevel;
	}


	//The boundry collider is sized and positioned according to gridSize.
	void SetupBoundary() {
		Transform boundaryTransform = boundary.GetComponent<Transform>();
		BoxCollider boundaryCollider = boundary.GetComponent<BoxCollider>();

		//Needed do some calculations here anyway, so use floats to force a type change
		boundaryTransform.position = new Vector3 (gridSize * 1.0f, 0.0f, gridSize * 1.0f);
		boundaryCollider.size = new Vector3 (gridSize * 2.0f, blockSpawnHeight * 1.0f, gridSize * 2.0f);
	}
		
	void SetupSpawners() {
		spawners = new GameObject[gridSize * gridSize];
		CreateGridOfColliders (blockSpawnHeight + 2.0f, "spawner");
	}

	void SetupDetectors () {
		for (; blockSpawnHeight > 0; blockSpawnHeight -= 2) {
			CreateGridOfColliders(blockSpawnHeight - 1, "detector");
		}
	}
		
	//Create a single level of colliders at the specified y co-ordinate.
	void CreateGridOfColliders (float y, string name){
		
		GameObject spawnersHolder = new GameObject (name + "sHolder");
		int spawnerCount = 0;

		//Set the size of the array. 
		//Putting them in an array in the first place so they can be randomly called later.
		detectors = new GameObject[gridSize * gridSize];

		//Nested loop to go through rows and colums of spawner positions
		for (int x = (gridSize * 2) - 1; x > 0; x -= 2) 
		{
			for (int z = (gridSize * 2) - 1; z > 0; z -= 2) 
			{
				//Instantiate a spawner prefab at a given location.
				GameObject instantiatedSpawner = Instantiate (spawner, new Vector3(x, y, z), Quaternion.identity) as GameObject;

				switch (name) {
				//If it's on the top (spawner) layer of colliders
				case ("spawner"):
					spawners [spawnerCount] = instantiatedSpawner;
					break;
			
				//Or if it's on any of the other (detector) layers.
				case ("detector"):
					spawnersHolder.name = name + " (level " + blockSpawnHeight / 2 + ")";
					detectors [spawnerCount] = instantiatedSpawner;
					break;
				}

				instantiatedSpawner.name = name + " (" + x + "," + y + "," + z + ")";
				instantiatedSpawner.transform.SetParent (spawnersHolder.transform);
				spawnerCount++;
			}
		}
	}

	void ConstrainBlocks (Transform parent, Transform child) {
		
	}
}
