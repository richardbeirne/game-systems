﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour {

	public Rigidbody rb;
	public float speed = 50.0f;
	public bool landed = false;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		rb.velocity = -transform.up * speed;
	}
		
	void OnCollisionEnter (Collision col) {
		Debug.Log (col.collider.name);
		if (col.gameObject.tag == "ground" || col.gameObject.tag == "settledBlock") {
			StopBlock ();
		}
	}

	public bool isLanded (){
		return landed;
	}

	void StopBlock () {
		rb.isKinematic = true;
		rb.gameObject.tag = "settledBlock";
		landed = true;
	}
}
