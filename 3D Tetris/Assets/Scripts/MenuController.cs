﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

	public GameObject flavourTextGO;
	public GameObject scoreInfoGO;
	private StateController stateController;

	private Text flavourText;
	private Text scoreInfo;

	// Use this for initialization
	void Start () {
		flavourText = flavourTextGO.GetComponent<Text> ();
		scoreInfo = scoreInfoGO.GetComponent<Text> ();

		scoreInfo.text = "Your highest score is " + stateController.gameStats.highScore + " on level " + stateController.gameStats.highLevel + "!";
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void StartButtonEnter () {
		flavourText.text = "WIPE YOUR HIGH SCORE AND START FRESH!";
	}

	public void LoadButtonEnter () {
		flavourText.text = "RESUME A GAME FROM YOUR LAST COMPLETED LEVEL";
	}

	public void ExitButtonEnter () {
		flavourText.text = "NO! PLEASE DON'T GO!";
	}

	public void ButtonExit () {
		flavourText.text = "";
	}

	public void StartButtonPressed () {
		Application.LoadLevel (1);
	}

	public void LoadButtonPressed () {
		Debug.Log ("Loading last level and starting the scene.");
	}

	public void ExitButtonPressed () {
		Application.Quit();
	}
}
