﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;

[Serializable]
public struct GameStatus {
	public string playerName;
	public int currentScore, currentLevel, highScore, highLevel;
}

public class StateController : MonoBehaviour {

	// Setup save directory and file name, 
	const string SAVE_FILENAME = "gameStats.txt";
	string filePath;
	public GameStatus gameStats;

	void Awake () {
		DontDestroyOnLoad (transform.gameObject);
	}

	void Start () {
		//Create a new struct to hold our player data, and assign file path
		gameStats = new GameStatus ();
		filePath = Application.persistentDataPath; 

		//Load player data, or create some if not exists (via FirstRun())
		LoadEvent ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnApplicationPause (bool isPaused) {
		if (isPaused) {
			Debug.Log ("I'm paused");
		} else {
			Debug.Log ("I've resumed");
		}
	}


	void OnApplicationQuit () {
		Debug.Log ("quit");
		SaveEvent ();
	}


	// fired when saved
	public void SaveEvent () {
		Debug.Log ("Save Clicked!");
		string serializedGameStatus = JsonUtility.ToJson (gameStats);
		File.WriteAllText (filePath + "/" + SAVE_FILENAME, serializedGameStatus);
	}

	//fired when loaded
	public void LoadEvent () {
		if (File.Exists (filePath + "/" + SAVE_FILENAME)) {
			string jsonGameStatus = File.ReadAllText (filePath + "/" + SAVE_FILENAME);
			gameStats = JsonUtility.FromJson<GameStatus> (jsonGameStatus);
		} else {
			Debug.Log ("File not found, assuming first run.");
			FirstRun ();
		}
	}

	void FirstRun () {
		gameStats.playerName = "Player";
		gameStats.currentScore = 0;
		gameStats.currentLevel = 1;
		gameStats.highScore = 0;
		gameStats.highLevel = 1;
	}
}
